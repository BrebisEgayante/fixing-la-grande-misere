low_army_professionalism = {
	mercenary_cost = -0.15
	mercenary_manpower = 0.15
}

high_army_professionalism = {
	fire_damage = 0.1
	shock_damage = 0.1
	siege_ability = 0.2
	drill_decay_modifier = -0.5
}

drilling_armies = { # Multiplied by percentage of forcelimit currently drilling
	yearly_army_professionalism = 0.04		# from 0.01
}

########################################
# Regiment Types Modifiers
########################################
regiment_drill_modifier = { # Used by individual regiments
	shock_damage = 0.1										# from 0.1
	fire_damage = 0.1										# from 0.1
	fire_damage_received = -0.25							# from 0.25
	shock_damage_received = -0.25							# from 0.25
}

army_drill_modifier = { # Used by an entire army, is the average of all subunits drill
	movement_speed = 0.2
}


blockaded = {
	regiment_recruit_speed = 0.2
	ship_recruit_speed = 0.2
	local_monthly_devastation = 0.25
	province_trade_power_modifier = -10
}