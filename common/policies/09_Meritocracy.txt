the_Meritocracy_act1 = {
	monarch_power = MIL


	potential = {
		has_idea_group = plutocracy_ideas
		has_idea_group = religious_ideas
	}
	
	allow = {
		full_idea_group = plutocracy_ideas
		full_idea_group = religious_ideas
	}

	manpower_recovery_speed = 0.15                    
	
	ai_will_do = {
		factor = 1
		
	}
}

the_Meritocracy_act2 = {
	monarch_power = MIL

	potential = {
		has_idea_group = plutocracy_ideas
		has_idea_group = humanist_ideas
	}
	
	allow = {
		full_idea_group = plutocracy_ideas
		full_idea_group = humanist_ideas
	}

	global_manpower_modifier = 0.15                       
	
	ai_will_do = {
		factor = 1
	
	}
}

the_Meritocracy_act3 = {
	monarch_power = ADM


	potential = {
		has_idea_group = plutocracy_ideas
		has_idea_group = exploration_ideas
	}
	
	allow = {
		full_idea_group = plutocracy_ideas
		full_idea_group = exploration_ideas
	}

	naval_forcelimit_modifier = 0.15      
	
	ai_will_do = {
		factor = 1
	}
}

the_Meritocracy_act4 = {
	monarch_power = ADM

	potential = {
		has_idea_group = plutocracy_ideas
		has_idea_group = expansion_ideas
	}
	
	allow = {
		full_idea_group = plutocracy_ideas
		full_idea_group = expansion_ideas
	}

	ship_power_propagation = 0.20        
	
	ai_will_do = {
		factor = 1

	}
}

the_Meritocracy_act5 = {
	monarch_power = ADM


	potential = {
		has_idea_group = plutocracy_ideas
		has_idea_group = economic_ideas
	}
	
	allow = {
		full_idea_group = plutocracy_ideas
		full_idea_group = economic_ideas
	}

	fort_maintenance_modifier = -0.33            
	
	ai_will_do = {
		factor = 1
                modifier = {
                        factor = 2
                        is_at_war = yes
                }			
	}
}

the_Meritocracy_act6 = {
	monarch_power = MIL

	potential = {
		has_idea_group = plutocracy_ideas
		has_idea_group = administrative_ideas
	}
	
	allow = {
		full_idea_group = plutocracy_ideas
		full_idea_group = administrative_ideas
	}

	artillery_bonus_vs_fort = 1              
	
	ai_will_do = {
		factor = 1
		
	}
}

the_Meritocracy_act7 = {
	monarch_power = ADM

	potential = {
		has_idea_group = plutocracy_ideas
		has_idea_group = theocracy_gov_ideas
	}
	
	allow = {
		full_idea_group = plutocracy_ideas
		full_idea_group = theocracy_gov_ideas
	}

	global_unrest = -3              
	
	ai_will_do = {
		factor = 1
	}
}

