the_Delay_engagement_act1 = {
	monarch_power = MIL


	potential = {
		has_idea_group = defensive_ideas
		has_idea_group = religious_ideas
	}
	
	allow = {
		full_idea_group = defensive_ideas
		full_idea_group = religious_ideas
	}

	war_exhaustion = -0.10                           
	
	ai_will_do = {
		factor = 1
                modifier = {
                        factor = 2
                        is_at_war = yes
                }			
	}
}

the_Delay_engagement_act2 = {
	monarch_power = MIL

	potential = {
		has_idea_group = defensive_ideas
		has_idea_group = humanist_ideas
	}
	
	allow = {
		full_idea_group = defensive_ideas
		full_idea_group = humanist_ideas
	}

	manpower_recovery_speed = 0.15                
	
	ai_will_do = {
		factor = 1
	
	}
}

the_Delay_engagement_act3 = {
	monarch_power = ADM


	potential = {
		has_idea_group = defensive_ideas
		has_idea_group = exploration_ideas
	}
	
	allow = {
		full_idea_group = defensive_ideas
		full_idea_group = exploration_ideas
	}

	sunk_ship_morale_hit_recieved = -0.15       
	
	ai_will_do = {
		factor = 1
                modifier = {
                        factor = 2
                        is_at_war = yes
                }			
	}
}

the_Delay_engagement_act4 = {
	monarch_power = MIL

	potential = {
		has_idea_group = defensive_ideas
		has_idea_group = expansion_ideas
	}
	
	allow = {
		full_idea_group = defensive_ideas
		full_idea_group = expansion_ideas
	}

	navy_tradition_decay = -0.015      
	
	ai_will_do = {
		factor = 1

	}
}

the_Delay_engagement_act5 = {
	monarch_power = ADM


	potential = {
		has_idea_group = defensive_ideas
		has_idea_group = economic_ideas
	}
	
	allow = {
		full_idea_group = defensive_ideas
		full_idea_group = economic_ideas
	}

	dip_advisor_cost = -0.25         
	
	ai_will_do = {
		factor = 1
                modifier = {
                        factor = 2
                        is_at_war = yes
                }			
	}
}

the_Delay_engagement_act6 = {
	monarch_power = ADM

	potential = {
		has_idea_group = defensive_ideas
		has_idea_group = administrative_ideas
	}
	
	allow = {
		full_idea_group = defensive_ideas
		full_idea_group = administrative_ideas
	}

	naval_forcelimit_modifier = 0.2                   
	
	ai_will_do = {
		factor = 1
		
	}
}

the_Delay_engagement_act7 = {
	monarch_power = MIL

	potential = {
		has_idea_group = defensive_ideas
		has_idea_group = theocracy_gov_ideas
	}
	
	allow = {
		full_idea_group = defensive_ideas
		full_idea_group = theocracy_gov_ideas
	}

	shock_damage_received = -0.125                   
	
	ai_will_do = {
		factor = 1
                modifier = {
                        factor = 2
                        is_at_war = yes
                }			
	}
}

