the_Continentalism_act1 = {
	monarch_power = ADM


	potential = {
		has_idea_group = trade_ideas
		has_idea_group = religious_ideas
	}
	
	allow = {
		full_idea_group = trade_ideas
		full_idea_group = religious_ideas
	}

	reform_progress_growth = 0.33        
	
	ai_will_do = {
		factor = 1
	}
}

the_Continentalism_act2 = {
	monarch_power = ADM

	potential = {
		has_idea_group = trade_ideas
		has_idea_group = humanist_ideas
	}
	
	allow = {
		full_idea_group = trade_ideas
		full_idea_group = humanist_ideas
	}

	state_maintenance_modifier = -0.5
	
	ai_will_do = {
		factor = 1
                modifier = {
                        factor = 2
                        is_at_war = yes
                }			
	}
}

the_Continentalism_act3 = {
	monarch_power = ADM


	potential = {
		has_idea_group = trade_ideas
		has_idea_group = exploration_ideas
	}
	
	allow = {
		full_idea_group = trade_ideas
		full_idea_group = exploration_ideas
	}

	global_prosperity_growth = 0.15
	
	ai_will_do = {
		factor = 1
	}
}

the_Continentalism_act4 = {
	monarch_power = ADM

	potential = {
		has_idea_group = trade_ideas
		has_idea_group = expansion_ideas
	}
	
	allow = {
		full_idea_group = trade_ideas
		full_idea_group = expansion_ideas
	}

	global_autonomy = -0.1
	
	ai_will_do = {
		factor = 1
	}
}

the_Continentalism_act5 = {
	monarch_power = ADM


	potential = {
		has_idea_group = trade_ideas
		has_idea_group = economic_ideas
	}
	
	allow = {
		full_idea_group = trade_ideas
		full_idea_group = economic_ideas
	}

	same_culture_advisor_cost = -0.20       
	
	ai_will_do = {
		factor = 1
	}
}

the_Continentalism_act6 = {
	monarch_power = ADM

	potential = {
		has_idea_group = trade_ideas
		has_idea_group = administrative_ideas
	}
	
	allow = {
		full_idea_group = trade_ideas
		full_idea_group = administrative_ideas
	}

	ae_impact = -0.15
	
	ai_will_do = {
		factor = 1
	}
}

the_Continentalism_act7 = {
	monarch_power = MIL

	potential = {
		has_idea_group = trade_ideas
		has_idea_group = innovativeness_ideas
	}
	
	allow = {
		full_idea_group = trade_ideas
		full_idea_group = innovativeness_ideas
	}

	land_morale = 0.15
	
	ai_will_do = {
		factor = 1
	}
}

the_Continentalism_act8 = {
	monarch_power = DIP

	potential = {
		has_idea_group = trade_ideas
		has_idea_group = aristocracy_ideas
	}
	
	allow = {
		full_idea_group = trade_ideas
		full_idea_group = aristocracy_ideas
	}

	monthly_splendor = 3                 
	
        ai_will_do = {
                factor = 1
				
        }
}

the_Continentalism_act9 = {
	monarch_power = DIP

	potential = {
		has_idea_group = trade_ideas
		has_idea_group = plutocracy_ideas
	}
	
	allow = {
		full_idea_group = trade_ideas
		full_idea_group = plutocracy_ideas
	}

	prestige_decay = -0.02                 
	
        ai_will_do = {
                factor = 1		
				
        }
}

the_Continentalism_act10 = {
	monarch_power = MIL

	potential = {
		has_idea_group = trade_ideas
		has_idea_group = offensive_ideas
	}
	
	allow = {
		full_idea_group = trade_ideas
		full_idea_group = offensive_ideas
	}

	reinforce_speed = 0.33                         
	
        ai_will_do = {
                factor = 1
				
        }
}

the_Continentalism_act11 = {
	monarch_power = MIL

	potential = {
		has_idea_group = trade_ideas
		has_idea_group = defensive_ideas
	}
	
	allow = {
		full_idea_group = trade_ideas
		full_idea_group = defensive_ideas
	}

	hostile_attrition = 1                                  
	
        ai_will_do = {
			factor = 1
                modifier = {
                        factor = 2
                        is_at_war = yes
                }	
        }
}

the_Continentalism_act12 = {
	monarch_power = DIP

	potential = {
		has_idea_group = trade_ideas
		has_idea_group = naval_ideas
	}
	
	allow = {
		full_idea_group = trade_ideas
		full_idea_group = naval_ideas
	}

	global_regiment_cost = -0.10        
	
	ai_will_do = {
		factor = 1

	}
}

the_Continentalism_act13 = {
	monarch_power = ADM

	potential = {
		has_idea_group = trade_ideas
		has_idea_group = theocracy_gov_ideas
	}
	
	allow = {
		full_idea_group = trade_ideas
		full_idea_group = theocracy_gov_ideas
	}

	institution_spread_from_true_faith = 1
	
	ai_will_do = {
		factor = 1
	}
}

the_Continentalism_act14 = {
	monarch_power = DIP

	potential = {
		has_idea_group = trade_ideas
		has_idea_group = quantity_ideas
	}
	
	allow = {
		full_idea_group = trade_ideas
		full_idea_group = quantity_ideas
	}

	prestige = 1.5
	
        ai_will_do = {
			factor = 1

        }
}

the_Continentalism_act15 = {
	monarch_power = DIP

	potential = {
		has_idea_group = trade_ideas
		has_idea_group = quality_ideas
	}
	
	allow = {
		full_idea_group = trade_ideas
		full_idea_group = quality_ideas
	}

	global_own_trade_power = 0.20            
	
        ai_will_do = {
                factor = 1
        }
}
