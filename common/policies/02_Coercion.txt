the_Coercion_act1 = {
	monarch_power = DIP


	potential = {
		has_idea_group = influence_ideas
		has_idea_group = religious_ideas
	}
	
	allow = {
		full_idea_group = influence_ideas
		full_idea_group = religious_ideas
	}

	reduced_liberty_desire = 15
	
	ai_will_do = {
		factor = 1
	}
}

the_Coercion_act2 = {
	monarch_power = DIP

	potential = {
		has_idea_group = influence_ideas
		has_idea_group = humanist_ideas
	}
	
	allow = {
		full_idea_group = influence_ideas
		full_idea_group = humanist_ideas
	}

	vassal_forcelimit_bonus = 0.5
	
	ai_will_do = {
		factor = 1
                modifier = {
                        factor = 2
                        is_at_war = yes
                }			
	}
}

the_Coercion_act3 = {
	monarch_power = ADM


	potential = {
		has_idea_group = influence_ideas
		has_idea_group = exploration_ideas
	}
	
	allow = {
		full_idea_group = influence_ideas
		full_idea_group = exploration_ideas
	}

	liberty_desire_from_subject_development = -0.20
	
	ai_will_do = {
		factor = 1
	}
}

the_Coercion_act4 = {
	monarch_power = ADM

	potential = {
		has_idea_group = influence_ideas
		has_idea_group = expansion_ideas
	}
	
	allow = {
		full_idea_group = influence_ideas
		full_idea_group = expansion_ideas
	}

	administrative_efficiency = 0.025
	
	ai_will_do = {
		factor = 1
	}
}

the_Coercion_act5 = {
	monarch_power = DIP


	potential = {
		has_idea_group = influence_ideas
		has_idea_group = economic_ideas
	}
	
	allow = {
		full_idea_group = influence_ideas
		full_idea_group = economic_ideas
	}

	yearly_absolutism = 1        
	
	ai_will_do = {
		factor = 1
	}
}

the_Coercion_act6 = {
	monarch_power = DIP

	potential = {
		has_idea_group = influence_ideas
		has_idea_group = administrative_ideas
	}
	
	allow = {
		full_idea_group = influence_ideas
		full_idea_group = administrative_ideas
	}

	max_absolutism = 10           
	
	ai_will_do = {
		factor = 1
	}
}

the_Coercion_act7 = {
	monarch_power = MIL

	potential = {
		has_idea_group = influence_ideas
		has_idea_group = innovativeness_ideas
	}
	
	allow = {
		full_idea_group = influence_ideas
		full_idea_group = innovativeness_ideas
	}

	yearly_army_professionalism = 0.015
	
	ai_will_do = {
		factor = 1
	}
}

the_Coercion_act8 = {
	monarch_power = DIP

	potential = {
		has_idea_group = influence_ideas
		has_idea_group = aristocracy_ideas
	}
	
	allow = {
		full_idea_group = influence_ideas
		full_idea_group = aristocracy_ideas
	}

	monthly_heir_claim_increase = 0.2           
	
        ai_will_do = {
                factor = 1
			
        }
}

the_Coercion_act9 = {
	monarch_power = DIP

	potential = {
		has_idea_group = influence_ideas
		has_idea_group = plutocracy_ideas
	}
	
	allow = {
		full_idea_group = influence_ideas
		full_idea_group = plutocracy_ideas
	}

	legitimacy = 1.5           
	
        ai_will_do = {
                factor = 1			
        }
}

the_Coercion_act10 = {
	monarch_power = DIP

	potential = {
		has_idea_group = influence_ideas
		has_idea_group = offensive_ideas
	}
	
	allow = {
		full_idea_group = influence_ideas
		full_idea_group = offensive_ideas
	}

	army_tradition = 1                         
	
        ai_will_do = {
                factor = 1
                modifier = {
                        factor = 2
                        is_at_war = yes
                }					
        }
}

the_Coercion_act11 = {
	monarch_power = DIP

	potential = {
		has_idea_group = influence_ideas
		has_idea_group = defensive_ideas
	}
	
	allow = {
		full_idea_group = influence_ideas
		full_idea_group = defensive_ideas
	}

	prestige_decay = -0.02                   
	
        ai_will_do = {
			factor = 1
        }
}

the_Coercion_act12 = {
	monarch_power = DIP

	potential = {
		has_idea_group = influence_ideas
		has_idea_group = naval_ideas
	}
	
	allow = {
		full_idea_group = influence_ideas
		full_idea_group = naval_ideas
	}

	advisor_cost = -0.10
	
	ai_will_do = {
		factor = 1
	}
}

the_Coercion_act13 = {
	monarch_power = ADM

	potential = {
		has_idea_group = influence_ideas
		has_idea_group = theocracy_gov_ideas
	}
	
	allow = {
		full_idea_group = influence_ideas
		full_idea_group = theocracy_gov_ideas
	}

	colonists = 1
	
	ai_will_do = {
		factor = 1
	}
}

the_Coercion_act14 = {
	monarch_power = MIL

	potential = {
		has_idea_group = influence_ideas
		has_idea_group = quantity_ideas
	}
	
	allow = {
		full_idea_group = influence_ideas
		full_idea_group = quantity_ideas
	}

	land_forcelimit_modifier = 0.15       
	
        ai_will_do = {
			factor = 1
        }
}

the_Coercion_act15 = {
	monarch_power = DIP

	potential = {
		has_idea_group = influence_ideas
		has_idea_group = quality_ideas
	}
	
	allow = {
		full_idea_group = influence_ideas
		full_idea_group = quality_ideas
	}

	global_spy_defence = 0.20     
	
        ai_will_do = {
                factor = 1
        }
}
