the_Drilling_standardisation_act1 = {
	monarch_power = MIL


	potential = {
		has_idea_group = quality_ideas
		has_idea_group = religious_ideas
	}
	
	allow = {
		full_idea_group = quality_ideas
		full_idea_group = religious_ideas
	}

	discipline = 0.05                               
	
	ai_will_do = {
		factor = 1
                modifier = {
                        factor = 2
                        is_at_war = yes
                }				
	}
}

the_Drilling_standardisation_act2 = {
	monarch_power = MIL

	potential = {
		has_idea_group = quality_ideas
		has_idea_group = humanist_ideas
	}
	
	allow = {
		full_idea_group = quality_ideas
		full_idea_group = humanist_ideas
	}

	yearly_army_professionalism = 0.02             
	
	ai_will_do = {
		factor = 1
                modifier = {
                        factor = 2
                        is_at_war = yes
                }		
	}
}

the_Drilling_standardisation_act3 = {
	monarch_power = MIL


	potential = {
		has_idea_group = quality_ideas
		has_idea_group = exploration_ideas
	}
	
	allow = {
		full_idea_group = quality_ideas
		full_idea_group = exploration_ideas
	}

	backrow_artillery_damage = 0.10             
	
	ai_will_do = {
		factor = 1
                modifier = {
                        factor = 2
                        is_at_war = yes
                }			
	}
}

the_Drilling_standardisation_act4 = {
	monarch_power = ADM

	potential = {
		has_idea_group = quality_ideas
		has_idea_group = expansion_ideas
	}
	
	allow = {
		full_idea_group = quality_ideas
		full_idea_group = expansion_ideas
	}

	siege_blockade_progress = 1                                     
	
	ai_will_do = {
		factor = 1
                modifier = {
                        factor = 2
                        is_at_war = yes
                }	
	}
}

the_Drilling_standardisation_act5 = {
	monarch_power = ADM


	potential = {
		has_idea_group = quality_ideas
		has_idea_group = economic_ideas
	}
	
	allow = {
		full_idea_group = quality_ideas
		full_idea_group = economic_ideas
	}

	tolerance_heretic = 3                   
	
	ai_will_do = {
		factor = 1
                modifier = {
                        factor = 2
                        is_at_war = yes
                }			
	}
}

the_Drilling_standardisation_act6 = {
	monarch_power = MIL

	potential = {
		has_idea_group = quality_ideas
		has_idea_group = administrative_ideas
	}
	
	allow = {
		full_idea_group = quality_ideas
		full_idea_group = administrative_ideas
	}

	cavalry_power = 0.15                                
	
	ai_will_do = {
		factor = 1
                modifier = {
                        factor = 2
                        is_at_war = yes
                }			
	}
}

the_Drilling_standardisation_act7 = {
	monarch_power = MIL

	potential = {
		has_idea_group = quality_ideas
		has_idea_group = theocracy_gov_ideas
	}
	
	allow = {
		full_idea_group = quality_ideas
		full_idea_group = theocracy_gov_ideas
	}

	infantry_power = 0.10                                 
	
	ai_will_do = {
		factor = 1
                modifier = {
                        factor = 2
                        is_at_war = yes
                }			
	}
}

