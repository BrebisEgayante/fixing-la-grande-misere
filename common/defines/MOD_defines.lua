-- Format for overwriting define values:
--
-- NDefines.NDiplomacy.MAX_CLIENT_STATES = 20

--  NDefines.NCountry.MINIMUM_POLICY_TIME = 3					-- was 10

NDefines.NCountry.MONTHS_FOR_MAX_MP_ALERT = 6				-- was 3
NDefines.NCountry.ESTATE_AGENDA_ABOUT_TO_EXPIRE_WARNING = 2190
NDefines.NCountry.ABANDON_IDEAGROUP_REFUND = 0.25			-- was 0.10		-- The part of the idea group spent that will be refunded upon abandonment.

--	NDefines.NCountry.PS_FACTION_BOOST = 50						-- was 10
--	NDefines.NCountry.CALL_ALLY_DECLINE_PRESTIGE_PENALTY = 0	-- was -25
--	NDefines.NCountry.CORE_LOSE_PRESTIGE = 0					-- was -10		-- Prestige change when lost core
--	NDefines.NCountry.ABANDON_CORE_PRESTIGE = 0					-- was -10		-- The cost of abandoning a core that some other country owns.

NDefines.NCountry.BASE_TARIFF = 0							-- was 0.10								-- Basic part of colonies income that goes to tariffs
NDefines.NCountry.DEBT_LIMIT_ADM = -999					-- was -100		-- This is how far you can go into debt on ADM for actions that allow spending power you don't have.
NDefines.NCountry.DEBT_LIMIT_DIP = -999									-- This is how far you can go into debt on DIP for actions that allow spending power you don't have.
NDefines.NCountry.DEBT_LIMIT_MIL = -999				-- was -100		-- This is how far you can go into debt on MIL for actions that allow spending power you don't have.

--  NDefines.NCountry.CONCENTRATE_DEVELOPMENT_DEVELOPMENT_DECREASE = 0		-- was 0.2
--	NDefines.NCountry.LIBERTY_DESIRE_MERCANTILISM = 0.25				-- Liberty desire from overlord's mercantilism (per Merc).
--	NDefines.NCountry.LIBERTY_DESIRE_ADM_EFFICIENCY = 0.2				-- Liberty desire per point of admistrative efficiency (colonies only)
--  NDefines.NCountry.DIVERT_TRADE_LIBERTY = 30
--	***	PILLAGE_CAPITAL_DEVELOPMENT_DECREASE = 0.2
--	***	PILLAGE_CAPITAL_MAX_DEV_PILLAGED = 2

NDefines.NCountry.PS_STRENGTHEN_GOVERNMENT = 75							-- was 100
NDefines.NCountry.CORE_HAD_PERMANENT_CLAIM = 0.1						--was 0.25 
NDefines.NCountry.CONCENTRATE_DEVELOPMENT_DEVELOPMENT_DECREASE = 0		-- désactive concentrate dev
NDefines.NCountry.PILLAGE_CAPITAL_DEVELOPMENT_DECREASE = 0				-- désactive pillage capital
NDefines.NCountry.PS_IMPROVE_PROVINCE_CAPITAL_DISCOUNT = 0.025			-- was 0.05

--  NDefines.NDiplomacy.PEACE_COST_PILLAGE_CAPITAL = 0					-- was 1

NDefines.NDiplomacy.MIN_RELATIONS_TO_ALLY = -200
NDefines.NDiplomacy.KNOWLEDGE_SHARING_INSTITUTION_GROWTH_MONTHLY = 0.25	-- # was 1 
NDefines.NDiplomacy.KNOWLEDGE_SHARING_DURATION_YEARS = 40				-- # was 10 
NDefines.NDiplomacy.MARCH_BASE_DEVELOPMENT_CAP = 500					-- # was 250
NDefines.NDiplomacy.ABANDON_UNION_PRESTIGE = 0							-- was -10
NDefines.NDiplomacy.INTEGRATE_UNION_MIN_YEARS = 25						-- was 50
NDefines.NDiplomacy.PAY_SUBJECT_DEBT_LIBERTY_DESIRE_REDUCTION = 1		-- was 5

--  NDefines.NDiplomacy.FORCE_BREAK_ALLIANCE_TRUCE_YEARS = 10	

-- NDefines.NEconomy.MINIMUM_INTERESTS = 2.0		-- was 1.0						-- _EDEF_MINIMUM_INTERESTS_
-- NDefines.NEconomy.BASE_INTERESTS = 5.0			-- was 4.0								-- Base interests

NDefines.NEconomy.EXPELLED_MINORITY_DEV_BONUS_FACTOR = 0.2			-- Bonus development on minority expulsion completion, multiplied by origin province development
NDefines.NEconomy.EXPELLING_MINORITY_COLONIST_COST_FACTOR =  0.5	-- Colonist maintenance cost factor when expelling minority
NDefines.NEconomy.EXPELLING_MINORITY_SETTLER_CHANCE_FACTOR = 0.01			-- was 0.001	-- Settler chance bonus when expelling minority multiplied by origin province development

-- NDefines.NEconomy.GOLD_MINE_SIZE = 55						--	was 40		

NDefines.NMilitary.SLACKEN_MANPOWER_INCREASE = 1.0			-- was 2.0 
NDefines.NMilitary.BASE_MP_TO_MANPOWER = 0.2				-- Was 0.25
NDefines.NMilitary.SLACKEN_AP_DROP = 0.1					-- was 0.05	
NDefines.NMilitary.SLACKEN_MANPOWER_INCREASE = 1			-- was 2
NDefines.NMilitary.MERCENARY_COMPANY_MAX_REGIMENTS = 20		-- Maximum regiments	# was 60 #
NDefines.NMilitary.WARGOAL_MAX_BONUS = 45					-- was 25
NDefines.NMilitary.LOOTED_MAX = 15							-- was 5
NDefines.NMilitary.MARINE_SHOCK_DAMAGE_TAKEN = -0.10		-- was 0.10
NDefines.NMilitary.REVANCHISM_DEVASTATION_IMPACT = -0.05	-- was 0.02			-- 100 revanschism is -2 a year.
NDefines.NMilitary.ARMY_ATTRITION_AT_SEA = 3				-- was 10
--	NDefines.NMilitary.GALLEY_COMBAT_WIDTH = 0.75			-- was 1
--	NDefines.NMilitary.FORT_DEVASTATION_IMPACT = -10,					-- multiplied by fortlevel/max fortlevel in area per year.

--		CONTROL_DEVASTATION_IMPACT = -1,				-- devastation recovery given by control

--  NDefines.NAI.CALL_IN_ALLIES_POWER_RATIO = 3.0		-- was 4.0 -- AI will only call in allies in an offensive war if their military power ratio to the enemy is less than this

--	NDefines.NAI.GOVERNING_CAPACITY_OVER_PERCENTAGE_TOLERATED = 0.5		-- was 0.2

--  NDefines.NAI.MIN_SCORE_TO_CONCENTRATE_DEVELOPMENT = 15		--was 1.5
--	***		PEACE_TERMS_PILLAGE_CAPITAL_MULT = 2.0

NDefines.NGovernment.RUSSIAN_ABILITY_BASE_GAIN = 2						-- was 3
NDefines.NGovernment.RUSSIAN_ABILITY_STRELTSY_SPAWN_SIZE = 0.1			-- was 0.2

NDefines.NEconomy.LAND_TECH_MAINTENANCE_IMPACT = 0.03             	-- 2% each tech increases it.
NDefines.NEconomy.NAVAL_TECH_MAINTENANCE_IMPACT = 0.03            	-- 2% each tech increases it.



--	INSTITUTION_BONUS_FROM_IMP_DEVELOPMENT = 5			

--	INSTITUTION_CAP_IMP_DEVELOPMENT = 10,
--	INSTITUTION_BASE_IMP_DEVELOPMENT = 30,
	
--	EMBRACE_INSTITUTION_COST = 2.5,				-- 2.5 per development (autonomy modified)
--	MAXIMUM_CONDOTTIERI = 20,					-- Base number of units you can rent out
--	CORRUPTION_COST = 0.05,						-- cost for monthly combat per development
	
--	ESTATE_AGENDA_DENIAL_LOYALTY_PENALTY = -5,	-- Loylaty penalty of denying an estate agenda
--	ESTATE_AGENDA_DEFAULT_MAX_ACTIVE_DAYS = 7300, -- Default max amount of days an agenda can stay active
--	ESTATE_AGENDA_ABOUT_TO_EXPIRE_WARNING = 1095, -- How many days it should start warning for agenda about to expire

--	MAX_WAR_EXHAUSTION = 20

--	PS_BUY_IDEA = 400
	
--	PS_CHANGE_CULTURE = 10,
--	PS_HARSH_TREATMENT_COST = 200
	
--	CORE_HAD_PERMANENT_CLAIM = 0.25
	
--	FACTION_BOOST_SIZE = 10
	
--	INITIAL_REGULAR_COLONY = 10,
--	REGULAR_COLONY_GROWTH = 25
	
--	OVEREXTENSION_OVERSEAS_FACTOR = 0.5	
	
--	MONTHS_TO_CHANGE_CULTURE = 10,
	
--	REVOLT_SIZE_BASE = 5,
--	REVOLT_TECH_IMPACT = 0.03, 			-- % each tech increases size of rebels by this percent.
--	REVOLT_TECH_MORALE = 0.01,			-- 1% per tech level

--	TARIFF_INCREASE_STEP = 0.05,					-- Increase on each boost
--	TARIFF_LIBERTY_INCREASE = 1.0,					-- Liberty increase for each % tariffs
--	TARIFF_DECREASE_STEP = -0.05
		
--	CONCENTRATED_DEV_LIBERTY_DESIRE_PER_DEV = 2
	
--	RAZE_PROVINCE_POWER_PER_DEVELOPMENT = 25.0,
	
--	TRADE_FAVORS_FOR_GOLD_FAVOR_COST = 10,
--	TRADE_FAVORS_FOR_GOLD_REQUIRED_OPINION = 50,
--	TRADE_FAVORS_FOR_MEN_FAVOR_COST = 10,
--	TRADE_FAVORS_FOR_MEN_REQUIRED_OPINION = 50,
--	TRADE_FAVORS_FOR_SAILORS_FAVOR_COST = 10,
--	TRADE_FAVORS_FOR_SAILORS_REQUIRED_OPINION = 50,
--	TRADE_FAVORS_FOR_HEIR_FAVOR_COST = 90,
--	TRADE_FAVORS_FOR_HEIR_REQUIRED_OPINION = 50,
--	TRADE_FAVORS_FOR_TRUST_REQUIRED_OPINION = 50,
--	TRADE_FAVORS_FOR_WAR_PREP_REQUIRED_OPINION = 50,
--	USING_FAVORS_AI_BOOST = 50,
	
--	GOLD_MINE_SIZE = 40,							-- Base income from gold mines

-- NDefines.NMilitary.SIEGE_ATTRITION = 1									-- was 1 # nic #

--	MERCENARY_COMPANY_MANPOWER_PER_REGIMENT = 2.0,	-- Manpower reserve factor (multiplied by 1000 per regiment)
--	MERCENARY_COMPANY_MANPOWER_RECOVERY = 120,				-- How many months to recover mercenary manpower fully
--	MERCENARY_COMPANY_HIRED_MANPOWER_RECOVERY_MOD = 2.0,		-- How much to modify the length for manpower recovery if they are hired
	
-- NAI	
--	PRESS_THEM_FURTHER = 0, -- This makes AI that has been promised land require that the enemy is pressed further if they think it is possible. Set to 1 to activate

--	MIN_CAV_PERCENTAGE = 5, --AI will always try to have at least this many % of their army as cav, regardless of time in the game.
--	MAX_CAV_PERCENTAGE = 50, -- For modding, actual ratio is dynamically computed but will be no higher than this.
--	FOG_OF_WAR_FORGET_CHANCE = 1, --Daily percentage chance of AI forgetting a unit that's hidden in Fog of War.
	
--	DEVELOPMENT_CAP_BASE = 10,	-- AI will not develop provinces that have more development than this or DEVELOPMENT_CAP_MULT*original development (whichever is bigger)
--	DEVELOPMENT_CAP_MULT = 2,
	
--	NReligion 	
--		MAYA_COLLAPSE_PROVINCES = 15,	-- Maya collapses to this size on reform
